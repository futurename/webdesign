
/* jquery init */
$(document).ready(function(){
  // slick slider
  $('.slider-photos').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 375, 
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      }
    ]
  });


});


